use bellman::{Circuit, ConstraintSystem, SynthesisError};

use bellman_circuit_exporter;

pub struct CircuitPointOnCurve {
    g: Option<jubjub::ExtendedPoint>,
}

pub struct CircuitPointDouble {
    g: Option<jubjub::ExtendedPoint>,
}

pub struct CircuitPointAdd {
    g1: Option<jubjub::ExtendedPoint>,
    g2: Option<jubjub::ExtendedPoint>,
}

impl Circuit<jubjub::Fq> for CircuitPointOnCurve {
    fn synthesize<CS: ConstraintSystem<jubjub::Fq>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        zcash_proofs::circuit::ecc::EdwardsPoint::witness(
            cs.namespace(|| "Witness point in the circuit"),
            self.g,
        )?;
        Ok(())
    }
}

impl Circuit<jubjub::Fq> for CircuitPointDouble {
    fn synthesize<CS: ConstraintSystem<jubjub::Fq>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        let p = zcash_proofs::circuit::ecc::EdwardsPoint::witness(
            cs.namespace(|| "Witness point in the circuit"),
            self.g,
        )?;
        p.double(cs.namespace(|| "Double the point"))?;
        Ok(())
    }
}

impl Circuit<jubjub::Fq> for CircuitPointAdd {
    fn synthesize<CS: ConstraintSystem<jubjub::Fq>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        let allocated_g1 = zcash_proofs::circuit::ecc::EdwardsPoint::witness(
            cs.namespace(|| "Witness g1 in the circuit"),
            self.g1,
        )?;
        let allocated_g2 = zcash_proofs::circuit::ecc::EdwardsPoint::witness(
            cs.namespace(|| "Witness g1 in the circuit"),
            self.g2,
        )?;
        allocated_g1.add(
            cs.namespace(|| "Add the points and create a new one"),
            &allocated_g2,
        )?;
        Ok(())
    }
}

pub fn main() {
    let circuit = CircuitPointOnCurve { g: None };
    bellman_circuit_exporter::export_to_json::<CircuitPointOnCurve, jubjub::Fq>(
        circuit,
        String::from("ecc-jubjub-point-on-curve.r1cs"),
        false,
    )
    .unwrap();

    let circuit = CircuitPointDouble { g: None };
    bellman_circuit_exporter::export_to_json::<CircuitPointDouble, jubjub::Fq>(
        circuit,
        String::from("ecc-jubjub-double.r1cs"),
        false,
    )
    .unwrap();

    let circuit = CircuitPointAdd { g1: None, g2: None };
    bellman_circuit_exporter::export_to_json::<CircuitPointAdd, jubjub::Fq>(
        circuit,
        String::from("ecc-jubjub-add.r1cs"),
        false,
    )
    .unwrap();
}
