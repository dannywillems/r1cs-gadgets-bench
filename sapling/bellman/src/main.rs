use bellman_circuit_exporter;
use bls12_381::Scalar;
use zcash_proofs::circuit::sapling::{Output, Spend};

pub fn main() {
    let circuit = Output {
        value_commitment: None,
        payment_address: None,
        commitment_randomness: None,
        esk: None,
    };

    bellman_circuit_exporter::export_to_json::<Output, bls12_381::Scalar>(
        circuit,
        String::from("sapling-output-circuit.r1cs"),
        false,
    )
    .unwrap();

    let spend = Spend {
        value_commitment: None,
        proof_generation_key: None,
        payment_address: None,
        commitment_randomness: None,
        ar: None,
        auth_path: vec![None],
        anchor: None,
    };

    bellman_circuit_exporter::export_to_json::<Spend, bls12_381::Scalar>(
        spend,
        String::from("sapling-spend-circuit.r1cs"),
        false,
    )
    .unwrap();
}
