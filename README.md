# r1cs-gadgets-bench

Bench of different r1cs gadgets in different languages and libraries

## Hash functions

|                          | Pedersen Hash on BLS12-381                |
| ------------------------ | ----------------------------------------- |
| Bellman/zcash_primitives | 3 publics, 697 witnesses, 699 constraints |


## Sapling circuits

|                          | Output                                      | Spend                                         |
| -                        | -                                           | -                                             |
| Bellman/zcash_primitives | 6 publics, 7821 witnesses, 7827 constraints | 8 publics, 55737 witnesses, 55845 constraints |
|                          |                                             |                                               |
