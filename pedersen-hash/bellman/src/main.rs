use zcash_primitives::pedersen_hash::Personalization;
use zcash_proofs::circuit::pedersen_hash as gadget_pedersen_hash;

use bellman::{
    gadgets::boolean::{AllocatedBit, Boolean},
    Circuit, ConstraintSystem, SynthesisError,
};

use bellman_circuit_exporter;
use bls12_381;

const input_size: usize = 256;

pub struct MyCircuit {
    preimage: Option<Vec<bool>>,
}

impl Circuit<bls12_381::Scalar> for MyCircuit {
    fn synthesize<CS: ConstraintSystem<bls12_381::Scalar>>(
        self,
        cs: &mut CS,
    ) -> Result<(), SynthesisError> {
        // Witness the bits of the preimage.
        let preimage_bits: Vec<Option<bool>> = if let Some(preimage) = self.preimage.clone() {
            preimage.into_iter().map(|b| Some(b)).collect()
        } else {
            // 180 (186 - 6) = preimage length (without the 6 first bits for the test vectors)
            vec![None; input_size]
        };
        // For each bit of the input, we allocate a private variable.
        let preimage_bits_private: Vec<Boolean> = preimage_bits
            .into_iter()
            .enumerate()
            // Allocate each bit.
            .map(|(i, b)| AllocatedBit::alloc(cs.namespace(|| format!("preimage bit {}", i)), b))
            // Convert the AllocatedBits into Booleans (required for the sha256 gadget).
            .map(|b| b.map(Boolean::from))
            .collect::<Result<Vec<_>, _>>()?;

        // Compute hash = pedersen(preimage). It adds the computation constraints.
        let hash = gadget_pedersen_hash::pedersen_hash(
            cs.namespace(|| "Pedersen hash"),
            Personalization::NoteCommitment,
            &preimage_bits_private,
        )?;

        // We add two constraints for the coordinates. One is enough as we are
        // in the subgroup.
        let u: &bellman::gadgets::num::AllocatedNum<bls12_381::Scalar> = hash.get_u();
        let v: &bellman::gadgets::num::AllocatedNum<bls12_381::Scalar> = hash.get_v();
        u.inputize(cs.namespace(|| "Inputize u coordinate"))?;
        v.inputize(cs.namespace(|| "Inputize v coordinate"))?;

        Ok(())
    }
}

pub fn main() {
    let circuit = MyCircuit { preimage: None };
    bellman_circuit_exporter::export_to_json::<MyCircuit, bls12_381::Scalar>(
        circuit,
        String::from("pedersen-hash.r1cs"),
        false,
    )
    .unwrap();
}
